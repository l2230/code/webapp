import streamlit as st

import numpy as np
import pandas as pd
import lateco.Items as items
import lateco.Tagsets as tagsets
from collections import Counter

wordinput = st.sidebar.text_input("A lemma:", value="gehen", key="word")

postag = st.sidebar.selectbox('Choose POS tag:',  tagsets.stts["all"], index=7)

def buttonClick():
    sc = items.Subcorpus("wiki_00")

    st.write("* Lemma: ", st.session_state.word , "\n* POS: ",  postag, "\n\n----------\n\n")

    for a in sc.articles:
        for s in a.sentences:  
            toDisplay = False         
            for t in s.tokens:                
                if t.lemma == st.session_state.word and t.pos == postag:
                   toDisplay = True
                   break
            if toDisplay:
                temp = ""
                for t in s.tokens:
                    if t.lemma ==   st.session_state.word:
                        temp = temp + "<span style='color:red'>" + t.token + "</span>" + " "
                    else:                  
                        temp = temp + t.token + " "                    
                #st.write(temp) 
                st.markdown(temp, unsafe_allow_html=True)
                st.write("------")

def list2df(count, title):
    df = pd.DataFrame(count.values(), index=count.keys())
    df = df.rename(columns = {list(df)[0]: title})
    df = df.sort_values(title, ascending=False)
    return df

def statisticsClick():
    sc = items.Subcorpus("wiki_00")

    pos = Counter()
    artTP = Counter()
    wordLength = Counter()

    count = 0
    for a in sc.articles:
        for s in a.sentences:
            foundArt = False
            for t in s.tokens:
                wordLength[len(t.token)] += 1
                if foundArt:
                    artTP[t.pos] += 1
                count = count + 1
                pos[t.pos] += 1
                if t.pos == "ART":
                    foundArt = True
                else:
                    foundArt = False
            
    st.write("Number of tokens: ", count)
    st.bar_chart(data=list2df(pos, "POS Tags"), width=0, height=0, use_container_width=True)
    st.bar_chart(data=list2df(artTP, "POS Count after ART"), width=0, height=0, use_container_width=True)
    st.bar_chart(data=list2df(wordLength, "Length of words vs. their frequency"), width=0, height=0, use_container_width=True)
    #notebookHelpers.plotIt(pos, "POS Count")
    #notebookHelpers.plotIt(artTP, 'POS Count after ART')
    #notebookHelpers.plotIt(wordLength, 'Length of words vs. their frequency')

add_button= st.sidebar.button("Go", on_click=buttonClick)
add_button= st.sidebar.button("Corpus Statistics", on_click=statisticsClick)



